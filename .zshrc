# Import aliases
source ~/.bashrc_aliases

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

source ~/Repos/powerlevel10k/powerlevel10k.zsh-theme

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh


export PATH=/home/berserk/.cargo/bin:$PATH
export OBSIDIAN_VAULT='/home/berserk/Documents/ObsidianVault'
export DOT='/home/berserk/Repos/dotfiles'

alias stowit='cd $DOT;stow -v -R -t ~ .;cd -'

alias v='vim'
typeset -g POWERLEVEL9K_INSTANT_PROMPT=quiet

bindkey -v
